sphtocart <- function(theta,phi){
    nsph =c(theta,phi)
  n <- c(0,0,0)
  n[1] <- sin(nsph[1]*pi/180.0)*cos(nsph[2]*pi/180.0)
  n[2] <- sin(nsph[1]*pi/180.0)*sin(nsph[2]*pi/180.0)
  n[3] <- cos(nsph[1]*pi/180.0)
  
  return(n)
}
