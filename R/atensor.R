#' Computes the 2nd Order Alignment Tensor
#'
#' This function computes the second order alignment tensor out of vectors.
#' The alignment tensors are the irreducible parts of the orientation tensors,
#' i.e. they are traceless.
#' The vectors need to be normalized and given as a table, where the columns 
#' are the x,y,z coefficients and each line is one vector.
#' @param the table of vectors (one per line)
#' @keywords alignment tensor
#' @export
#' @examples
#' atensor(cartN)

atensor <- function(cartN){
  at <- matrix(0,3,3)
  for (i in 1:length(cartN[,1]))
     {
       nn <- outer(cartN[i,],cartN[i,],FUN="*")
                                        #print(nn)
       tr <- nn[1,1]+nn[2,2]+nn[3,3]
       nn <- nn - 1.0/3.0*tr*diag(1,3)
                                        #print(nn)
       at <- at+nn
     }

  at <- 1.0/length(cartN[,1])*at
  at
}

