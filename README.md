![1410200024-alignment-1.jpg](https://bitbucket.org/repo/zy8bzp/images/2563743095-1410200024-alignment-1.jpg)

alignment package for R 
====
Developed by the VisPar Group of the Laboratory of Nonlinear Dynamics, Institute of Cybernetics at Tallinn University of Technology, alignment is an R package for calculating orientation parameters, orientation tensors, alignment tensors and the orientation distribution function for short fibres. This package is mainly used within our research on Steel Fibre Reinforced Cementitious Composites.

Usage
----
```
#!r
  sourceDir <- function(path, trace = TRUE, ...) {
    for (nm in list.files(path, pattern = "\\.[RrSsQq]$")) {
       if(trace) cat(nm,":")           
       source(file.path(path, nm), ...)
       if(trace) cat("\n")
    }
  }
```


To import all functions in the directory:
```
#!r
  sourceDir("/path/to/alignment/R")
```

Or, with devtools installed:
```
#!r
install_bitbucket(alignment, vispar, ref = "master", branch = NULL,
  auth_user = NULL, password = NULL, ...)
```



Citation
----
This package is based on code and functions originally developed for the following publications (the functions have been re-organized), please cite these if you use the package:

Jussi-Petteri Suuronen, Aki Kallonen, Marika Eik, Jari Puttonen, Ritva Serimaa, and Heiko Herrmann. Analysis of short fibres orientation in steel fibre reinforced concrete (SFRC) using x-ray tomography. Journal of Materials Science, 48(3):1358-1367, February 2013

Marika Eik, Karl Lõhmus, Martin Tigasson, Madis Listak, Jari Puttonen, and Heiko Herrmann. DC-conductivity testing combined with photometry for measuring fibre orientations in SFRC. Journal of Materials Science, 48(10):3745-3759, May 2013

Heiko Herrmann, Marika Eik, Viktoria Berg, and Jari Puttonen. Phenomenological and numerical modelling of short fibre reinforced cementitious composites. Meccanica, 49(8):1985-2000, August 2014

____
Needs: GNU R 

Needs R packages: akima, ggplot2, abind

Suggests R packages: plotrix